# RTT Go

[Realtime train](https://www.realtimetrains.co.uk/) departure board, written in [Go](https://golang.org).

![Screenshot](https://bitbucket.org/repo/KnxkMzL/images/590491201-image.png)

### Usage

1. create a `.env` file based on `.env.example` file
2. run `docker-compose up` to build, test, and run the app