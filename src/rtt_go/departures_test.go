package main

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestBoardDepartureSummary(test *testing.T) {
	test.Run("Title()", func(test *testing.T) {
		test.Run("outside DST", func(test *testing.T) {
			subject := BoardDepartureSummary{
				DepartureResponse: DepartureResponse{
					Location: JourneyPointSkeleton{Name: "My Station"},
					Time:     time.Unix(1234567890, 0).In(TheTimeZone),
				},
			}

			assert.Equal(test, "Departures from My Station on Feb 13 2009 at 23:31 GMT", subject.Title())
		})

		test.Run("inside DST", func(test *testing.T) {
			subject := BoardDepartureSummary{
				DepartureResponse: DepartureResponse{
					Location: JourneyPointSkeleton{Name: "My Station"},
					Time:     time.Unix(1598268896, 0).In(TheTimeZone),
				},
			}

			assert.Equal(test, "Departures from My Station on Aug 24 2020 at 12:34 BST", subject.Title())
		})
	})

	test.Run("SummariesHTML()", func(test *testing.T) {
		test.Skip()

		subject := BoardDepartureSummary{
			DepartureResponse: DepartureResponse{},
		}

		assert.Equal(test, "...", subject.SummariesHTML())
	})
}
func TestDepartures(test *testing.T) {
	test.Run("IsBus()", func(test *testing.T) {
		test.Run("for trains", func(test *testing.T) {
			subject := Departure{ServiceType: "TRAIN"}

			assert.False(test, subject.IsBus())
		})

		test.Run("for buses", func(test *testing.T) {
			subject := Departure{ServiceType: "BUS"}

			assert.True(test, subject.IsBus())
		})

		test.Run("for unknowns", func(test *testing.T) {
			subject := Departure{ServiceType: ""}

			assert.False(test, subject.IsBus())
		})
	})
}

func TestTrainOperatingCompany(test *testing.T) {
	test.Run("AvgDelayMins()", func(test *testing.T) {
		test.Run("for zero services", func(test *testing.T) {
			subject := TrainOperatingCompany{DelayMins: 23.45, ServiceCount: 0}

			assert.Equal(test, 0, subject.AvgDelayMins())
		})

		test.Run("for one service", func(test *testing.T) {
			subject := TrainOperatingCompany{DelayMins: 23.45, ServiceCount: 1}

			assert.Equal(test, 23, subject.AvgDelayMins())
		})

		test.Run("for many services", func(test *testing.T) {
			subject := TrainOperatingCompany{DelayMins: 23.45, ServiceCount: 2}

			assert.Equal(test, 11, subject.AvgDelayMins())
		})
	})
}
