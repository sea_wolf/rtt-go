package main

import (
	"github.com/jedib0t/go-pretty/table"
	"github.com/newrelic/go-agent/v3/newrelic"
)

func board(crs string, theTime Clock, HTTPClient Connection, txn *newrelic.Transaction) DepartureResponse {
	currentTime := theTime.Time()
	departures := departures(crs, currentTime, HTTPClient, txn)
	return departures
}

func boardHTML(departures DepartureResponse) string {
	t := table.NewWriter()
	t.SetHTMLCSSClass("table table-striped table-bordered")

	var row table.Row

	row = table.Row{
		"Time",
		"Destination",
		"Exp.",
		"Plat.",
		"Operator"}
	t.AppendHeader(row)

	if len(departures.Services) == 0 {
		row = table.Row{
			"",
			"No departures",
			"",
			"",
			""}

		t.AppendRow(row)
	}

	for _, rttDeparture := range departures.Services {
		boardDeparture := BoardDeparture{rttDeparture}
		t.AppendRow(table.Row{
			boardDeparture.scheduled(),
			boardDeparture.statuses(),
			boardDeparture.expected(),
			boardDeparture.platform(),
			boardDeparture.tocName()})
	}

	html := t.RenderHTML()

	return html
}
