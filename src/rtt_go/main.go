package main

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"text/template"

	"github.com/newrelic/go-agent/v3/newrelic"
)

type page struct {
	CRS      string
	Date     string
	Time     string
	Timezone string
	Title    string
	Summary  string
	Body     string
}

func main() {
	app := newrelicApp()

	http.HandleFunc(newrelic.WrapHandleFunc(app, "/", handler))

	binding := fmt.Sprintf(":%s", os.Getenv("PORT"))
	log.Fatal(http.ListenAndServe(binding, nil))
}

func handler(response http.ResponseWriter, request *http.Request) {
	urlData, e := url.Parse(request.URL.String())
	if e != nil {
		panic(e)
	}
	query := urlData.Query()

	crs := query.Get("crs")
	date := query.Get("date")
	time := query.Get("time")

	if isCRS(crs) {
		response.WriteHeader(http.StatusOK)
		handleSearch(request, response, crs, date, time)
	} else {
		response.WriteHeader(http.StatusNotFound)
		handleError(crs)
	}
}

func handleSearch(request *http.Request, response http.ResponseWriter, crs string, dateStr string, timeStr string) {
	txn := newrelic.FromContext(request.Context())

	departures := searchDepartures(request, response, crs, dateStr, timeStr)
	departuresCount := len(departures.Services)
	txn.AddAttribute("Services", departuresCount)
	log.Printf(`HTTP:200 | crs:%s date:%s time:%s | services:%d`, crs, dateStr, timeStr, departuresCount)

	summary := BoardDepartureSummary{departures}
	segmentRenderBoard := txn.StartSegment("Render")
	displayedDate := TheTime.Format("2006-01-02")
	displayedTime := TheTime.Format("15:04")
	body := page{
		CRS:     crs,
		Date:    displayedDate,
		Time:    displayedTime,
		Title:   summary.Title(),
		Summary: summary.SummariesHTML(),
		Body:    boardHTML(summary.DepartureResponse),
	}

	file, e := Asset("view.html")
	if e != nil {
		panic(e)
	}

	content := string(file)
	t, e := template.New("view.html").Parse(content)
	if e != nil {
		panic(e)
	}

	t.Execute(response, body)
	segmentRenderBoard.End()
}

func handleError(crs string) {
	log.Printf(`HTTP:404 | "/%s"`, crs)
}
