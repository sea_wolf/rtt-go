package main

import "fmt"

// BoardDepartureSummary is an adaptor between Board and DepartureSummary
type BoardDepartureSummary struct {
	DepartureResponse DepartureResponse
}

// Title that describes what the DepartureResponse has queried
func (departureResponseSummary BoardDepartureSummary) Title() string {
	return fmt.Sprintf("Departures from %s on %s at %s",
		departureResponseSummary.DepartureResponse.Location.Name,
		departureResponseSummary.DepartureResponse.Time.Format("Jan 02 2006"),
		departureResponseSummary.DepartureResponse.Time.Format("15:04 MST"))
}

// SummariesHTML describes how traffic is running, per-TOC
func (departureResponseSummary BoardDepartureSummary) SummariesHTML() string {
	html := "<ul>"

	for _, summary := range departureResponseSummary.summaries() {
		html += "<li>" + summary + "</li>"
	}

	return html + "</ul>"
}

func (departureResponseSummary BoardDepartureSummary) summaries() []string {
	summaries := make([]string, 0)

	for _, toc := range departureResponseSummary.DepartureResponse.summaries() {
		summary := ""
		delayMins := toc.AvgDelayMins()
		if toc.HasCancellations() {
			summary = fmt.Sprintf("Services by %s are experiencing cancellations.", toc.Name)
		} else if delayMins < 3 {
			summary = fmt.Sprintf("Services by %s are running to time.", toc.Name)
		} else if delayMins < 5 {
			summary = fmt.Sprintf("Services by %s are lightly delayed (%d mins).", toc.Name, delayMins)
		} else {
			summary = fmt.Sprintf("Services by %s are heavily delayed (%d mins).", toc.Name, delayMins)
		}

		summaries = append(summaries, summary)
	}

	return summaries
}
