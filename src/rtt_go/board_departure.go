package main

import "strings"

// BoardDeparture is an adaptor between Board and Departure
type BoardDeparture struct {
	Departure Departure
}

func (boardDeparture BoardDeparture) statuses() string {
	statuses := make([]string, 0)
	if boardDeparture.Departure.Location.TerminatesHere() {
		statuses = append(statuses, "--- TERMINATES HERE ---")
	} else {
		statuses = append(statuses, boardDeparture.Departure.Location.AllDestinationNames())
	}

	if boardDeparture.Departure.Location.IsCancelled() {
		statuses = append(statuses, "· Cancelled due to "+boardDeparture.Departure.Location.Cancellation)
	}
	if !boardDeparture.Departure.Location.IsCancelled() && boardDeparture.Departure.Location.PlatformChanged && !boardDeparture.Departure.Location.TerminatesHere() {
		statuses = append(statuses, "» This is a platform alteration.")
	}
	if boardDeparture.Departure.Location.TerminatesHere() {
		statuses = append(statuses, "· Service from "+boardDeparture.Departure.Location.AllOriginNames())
	}

	return strings.Join(statuses, "\n")
}

func (boardDeparture BoardDeparture) scheduled() string {
	return boardDeparture.Departure.Location.DepartureScheduled.Format("15:04")
}

func (boardDeparture BoardDeparture) expected() string {
	if boardDeparture.Departure.Location.IsCancelled() {
		return "CANCELLED"
	} else if boardDeparture.Departure.Location.RightTimeDeparture() {
		return "On Time"
	} else {
		return boardDeparture.Departure.Location.DepartureActual.Format("15:04")
	}
}

func (boardDeparture BoardDeparture) platform() string {
	if boardDeparture.Departure.Location.IsCancelled() {
		return "─"
	} else if boardDeparture.Departure.IsBus() {
		return "(BUS)"
	} else if len(boardDeparture.Departure.Location.Platform) == 0 {
		return "─"
	} else {
		return boardDeparture.Departure.Location.Platform
	}
}

func (boardDeparture BoardDeparture) tocName() string {
	return boardDeparture.Departure.TOCName
}

func (boardDeparture BoardDeparture) isBus() bool {
	return strings.ToUpper(boardDeparture.Departure.ServiceType) == "BUS"
}
