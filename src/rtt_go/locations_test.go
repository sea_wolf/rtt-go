package main

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestCallingPoints(test *testing.T) {
	test.Run("AllOriginNames()", func(test *testing.T) {
		test.Run("for zero origins", func(test *testing.T) {
			subject := CallingPoint{Origins: []JourneyPoint{}}

			assert.Equal(test, "", subject.AllOriginNames())
		})

		test.Run("for one origin", func(test *testing.T) {
			subject := CallingPoint{Origins: []JourneyPoint{JourneyPoint{Name: "My Station"}}}

			assert.Equal(test, "My Station", subject.AllOriginNames())
		})

		test.Run("for many origins", func(test *testing.T) {
			subject := CallingPoint{Origins: []JourneyPoint{JourneyPoint{Name: "My Station"}, JourneyPoint{Name: "Your Station"}}}

			assert.Equal(test, "My Station, Your Station", subject.AllOriginNames())
		})
	})

	test.Run("AllDestinationNames()", func(test *testing.T) {
		test.Run("for zero destinations", func(test *testing.T) {
			subject := CallingPoint{Destinations: []JourneyPoint{}}

			assert.Equal(test, "", subject.AllDestinationNames())
		})

		test.Run("for one destination", func(test *testing.T) {
			subject := CallingPoint{Destinations: []JourneyPoint{JourneyPoint{Name: "My Station"}}}

			assert.Equal(test, "My Station", subject.AllDestinationNames())
		})

		test.Run("for many destinations", func(test *testing.T) {
			subject := CallingPoint{Destinations: []JourneyPoint{JourneyPoint{Name: "My Station"}, JourneyPoint{Name: "Your Station"}}}

			assert.Equal(test, "My Station, Your Station", subject.AllDestinationNames())
		})
	})

	test.Run("StartsHere()", func(test *testing.T) {
		test.Run("when the Origin", func(test *testing.T) {
			subject := CallingPoint{Name: "Here", Origins: []JourneyPoint{JourneyPoint{Name: "Here"}}}

			assert.True(test, subject.StartsHere())
		})

		test.Run("when included in the Origins", func(test *testing.T) {
			subject := CallingPoint{Name: "Here", Origins: []JourneyPoint{JourneyPoint{Name: "Here"}, JourneyPoint{Name: "There"}}}

			assert.True(test, subject.StartsHere())
		})

		test.Run("when not included in the Origins", func(test *testing.T) {
			subject := CallingPoint{Name: "Here", Origins: []JourneyPoint{JourneyPoint{Name: "There"}}}

			assert.False(test, subject.StartsHere())
		})

		test.Run("when there are no Origins", func(test *testing.T) {
			subject := CallingPoint{Name: "Here", Origins: []JourneyPoint{}}

			assert.False(test, subject.StartsHere())
		})
	})

	test.Run("TerminatesHere()", func(test *testing.T) {
		test.Run("when the Destinations", func(test *testing.T) {
			subject := CallingPoint{Name: "Here", Destinations: []JourneyPoint{JourneyPoint{Name: "Here"}}}

			assert.True(test, subject.TerminatesHere())
		})

		test.Run("when included in the Destinations", func(test *testing.T) {
			subject := CallingPoint{Name: "Here", Destinations: []JourneyPoint{JourneyPoint{Name: "Here"}, JourneyPoint{Name: "There"}}}

			assert.True(test, subject.TerminatesHere())
		})

		test.Run("when not included in the Destinations", func(test *testing.T) {
			subject := CallingPoint{Name: "Here", Destinations: []JourneyPoint{JourneyPoint{Name: "There"}}}

			assert.False(test, subject.TerminatesHere())
		})

		test.Run("when there are no Destinations", func(test *testing.T) {
			subject := CallingPoint{Name: "Here", Destinations: []JourneyPoint{}}

			assert.False(test, subject.TerminatesHere())
		})
	})

	test.Run("RightTimeArrival()", func(test *testing.T) {
		test.Run("when the times agree", func(test *testing.T) {
			subject := CallingPoint{ArrivalScheduled: TimeHHMMSS{Time: time.Unix(123, 0)}, ArrivalActual: TimeHHMMSS{Time: time.Unix(123, 0)}}

			assert.True(test, subject.RightTimeArrival())
		})

		test.Run("when the times disagree", func(test *testing.T) {
			subject := CallingPoint{ArrivalScheduled: TimeHHMMSS{Time: time.Unix(123, 0)}, ArrivalActual: TimeHHMMSS{Time: time.Unix(321, 0)}}

			assert.False(test, subject.RightTimeArrival())
		})

		test.Run("when there is no actual time", func(test *testing.T) {
			subject := CallingPoint{ArrivalScheduled: TimeHHMMSS{Time: time.Unix(123, 0)}}

			assert.False(test, subject.RightTimeArrival())
		})
	})

	test.Run("RightTimeDeparture()", func(test *testing.T) {
		test.Run("when the times agree", func(test *testing.T) {
			subject := CallingPoint{DepartureScheduled: TimeHHMMSS{Time: time.Unix(123, 0)}, DepartureActual: TimeHHMMSS{Time: time.Unix(123, 0)}}

			assert.True(test, subject.RightTimeDeparture())
		})

		test.Run("when the times disagree", func(test *testing.T) {
			subject := CallingPoint{DepartureScheduled: TimeHHMMSS{Time: time.Unix(123, 0)}, DepartureActual: TimeHHMMSS{Time: time.Unix(321, 0)}}

			assert.False(test, subject.RightTimeDeparture())
		})

		test.Run("when there is no actual time", func(test *testing.T) {
			subject := CallingPoint{DepartureScheduled: TimeHHMMSS{Time: time.Unix(123, 0)}}

			assert.False(test, subject.RightTimeDeparture())
		})
	})
}
