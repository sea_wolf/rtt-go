package main

import (
	"fmt"
	"net/http"
	"regexp"
	"time"

	"github.com/newrelic/go-agent/v3/newrelic"
)

var TheTime Clock
var HTTPClient Connection

var TheTimeZone, _ = time.LoadLocation("Europe/London")

func init() {
	TheTime = RealClock{time.Now().In(TheTimeZone)}
	HTTPClient = RealConnection{}
}

func searchDepartures(request *http.Request, response http.ResponseWriter, crs string, dateStr string, timeStr string) DepartureResponse {
	txn := newrelic.FromContext(request.Context())
	txn.AddAttribute("CRS", crs)
	txn.AddAttribute("Date", dateStr)
	txn.AddAttribute("Time", timeStr)

	if len(dateStr) > 0 && len(timeStr) > 0 {
		newTime, err := time.Parse("2006-01-02 15:04", fmt.Sprintf(`%s %s`, dateStr, timeStr))
		if err == nil {
			newTime = time.Date(
				newTime.Year(), newTime.Month(), newTime.Day(),
				newTime.Hour(), newTime.Minute(), newTime.Second(), 0,
				TheTimeZone,
			)
			TheTime = RealClock{newTime.In(TheTimeZone)}
		}
	} else {
		TheTime = RealClock{time.Now().In(TheTimeZone)}
	}

	return board(crs, TheTime, HTTPClient, txn)
}

func isCRS(str string) bool {
	matched, _ := regexp.MatchString(`^[A-Za-z]{3,7}$`, str)

	return matched
}
