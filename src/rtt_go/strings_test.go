package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTimeWithColon(test *testing.T) {
	assert := assert.New(test)

	test.Run("adds a colon to a string", func(test *testing.T) {
		subject := timeWithColon("12:34")
		expected := "12:34"
		assert.Equal(expected, subject, "they should be equal")
	})

	test.Run("returns an already-colon-containing string", func(test *testing.T) {
		subject := timeWithColon("1234")
		expected := "12:34"
		assert.Equal(expected, subject, "they should be equal")
	})

	test.Run("returns an already-empty string", func(test *testing.T) {
		subject := timeWithColon("")
		expected := ""
		assert.Equal(expected, subject, "they should be equal")
	})
}
