package main

import (
	"regexp"
	"strings"
	"time"
)

type TimeHHMMSS struct {
	time.Time

	Parsed bool
}

func (t *TimeHHMMSS) UnmarshalJSON(data []byte) error {
	str := strings.Trim(string(data), "\"")

	if matched, _ := regexp.MatchString(`^[0-9]{4}$`, str); matched {
		str = str + "00"
	}

	value, _ := time.Parse("150405", str)
	if !value.IsZero() {
		t.Time = value
	}

	return nil
}

func (t *TimeHHMMSS) Format(format string) string {
	if t.Time.IsZero() {
		return "─"
	}

	return t.Time.Format(format)
}

func (t *TimeHHMMSS) ToString() string {
	return t.Format("150405")
}
