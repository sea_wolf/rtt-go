package main

import (
	"io/ioutil"
	"testing"
	"time"

	"github.com/joho/godotenv"
	"github.com/newrelic/go-agent/v3/newrelic"
	"github.com/stretchr/testify/assert"
)

type fakeClock struct {
	theTime time.Time
}

func (fakeClock) Now() time.Time {
	t, _ := time.Parse(time.RFC3339, "2021-01-02T17:08:59+00:00")
	return t
}

func (c fakeClock) Format(fmt string) string {
	return c.Format(fmt)
}

func (c fakeClock) In(location *time.Location) time.Time {
	return c.Now().In(location)
}

func (c fakeClock) SetTime(newTime time.Time) {
	c.theTime = newTime
}

func (c fakeClock) Time() time.Time {
	return c.Now()
}

type fakeConnection struct{}

func (fakeConnection) DownloadDepartures(map[string]string, *newrelic.Transaction) string {
	data, _ := ioutil.ReadFile("../tests/departures/valid.json")
	return string(data)
}

type fakeEmptyConnection struct{}

func (fakeEmptyConnection) DownloadDepartures(map[string]string, *newrelic.Transaction) string {
	data, _ := ioutil.ReadFile("../tests/departures/no_services.json")
	return string(data)
}

type fakeBlankConnection struct{}

func (fakeBlankConnection) DownloadDepartures(map[string]string, *newrelic.Transaction) string {
	return ""
}

func TestBoard(test *testing.T) {
	godotenv.Overload(".env.test")
	assert := assert.New(test)

	TheTime = fakeClock{}
	HTTPClient = fakeConnection{}
	txn := newrelic.Transaction{}
	board := board("FTN", TheTime, HTTPClient, &txn)

	test.Run("parses the Location", func(test *testing.T) {
		subject := board

		assert.Equal("FTN", subject.Location.CRS)
		assert.Equal("Fratton", subject.Location.Name)
	})

	test.Run("parses the Departures", func(test *testing.T) {
		subject := board.Services[0]
		assert.Equal("Q31068", subject.UID)
		assert.Equal("1T62", subject.Headcode)
		assert.Equal("SW", subject.TOC)
		assert.Equal("South Western Railway", subject.TOCName)
		assert.Equal("train", subject.ServiceType)
		assert.True(subject.IsPassenger)
		assert.False(subject.IsBus())
		assert.Equal("FTN", subject.Location.CRS)
		assert.Equal("1", subject.Location.Platform)
		assert.Equal(true, subject.Location.PlatformConfirmed)
		assert.Equal(true, subject.Location.PlatformChanged)
		assert.Equal("170500", subject.Location.ArrivalScheduled.ToString())
		assert.Equal("170400", subject.Location.ArrivalActual.ToString())
		assert.Equal("170600", subject.Location.DepartureScheduled.ToString())
		assert.Equal("170600", subject.Location.DepartureActual.ToString())
		assert.Equal("Portsmouth Harbour", subject.Location.AllOriginNames())
		assert.Equal("Portsmouth Harbour", subject.Location.Origins[0].Name)
		assert.Equal("165700", subject.Location.Origins[0].Time.ToString())
		assert.Equal("Basingstoke", subject.Location.AllDestinationNames())
		assert.Equal("Basingstoke", subject.Location.Destinations[0].Name)
		assert.Equal("181500", subject.Location.Destinations[0].Time.ToString())
		assert.False(subject.Location.StartsHere())
		assert.False(subject.Location.TerminatesHere())
		assert.False(subject.Location.RightTimeArrival())
		assert.True(subject.Location.RightTimeDeparture())
		assert.False(subject.Location.IsCancelled())
	})

	test.Run("parses right-time arrivals/departures", func(test *testing.T) {
		subject := board.Services[1]
		assert.Equal("Q31059", subject.UID)
		assert.Equal("1T45", subject.Headcode)
		assert.Equal("SW", subject.TOC)
		assert.Equal("South Western Railway", subject.TOCName)
		assert.Equal("train", subject.ServiceType)
		assert.True(subject.IsPassenger)
		assert.False(subject.IsBus())
		assert.Equal("FTN", subject.Location.CRS)
		assert.Equal("3", subject.Location.Platform)
		assert.Equal(true, subject.Location.PlatformConfirmed)
		assert.Equal(false, subject.Location.PlatformChanged)
		assert.Equal("170800", subject.Location.ArrivalScheduled.ToString())
		assert.Equal("170700", subject.Location.ArrivalActual.ToString())
		assert.Equal("170800", subject.Location.DepartureScheduled.ToString())
		assert.Equal("170800", subject.Location.DepartureActual.ToString())
		assert.Equal("Basingstoke", subject.Location.AllOriginNames())
		assert.Equal("Basingstoke", subject.Location.Origins[0].Name)
		assert.Equal("160000", subject.Location.Origins[0].Time.ToString())
		assert.Equal("Portsmouth Harbour", subject.Location.AllDestinationNames())
		assert.Equal("Portsmouth Harbour", subject.Location.Destinations[0].Name)
		assert.Equal("171800", subject.Location.Destinations[0].Time.ToString())
		assert.False(subject.Location.StartsHere())
		assert.False(subject.Location.TerminatesHere())
		assert.False(subject.Location.RightTimeArrival())
		assert.True(subject.Location.RightTimeDeparture())
		assert.False(subject.Location.IsCancelled())
	})

	test.Run("supports confirmed and changed platforms", func(test *testing.T) {
		subject := board.Services[2]
		assert.Equal("X14020", subject.UID)
		assert.Equal("1J53", subject.Headcode)
		assert.Equal("SN", subject.TOC)
		assert.Equal("Southern", subject.TOCName)
		assert.Equal("train", subject.ServiceType)
		assert.True(subject.IsPassenger)
		assert.False(subject.IsBus())
		assert.Equal("FTN", subject.Location.CRS)
		assert.Equal("1", subject.Location.Platform)
		assert.Equal(true, subject.Location.PlatformConfirmed)
		assert.Equal(false, subject.Location.PlatformChanged)
		assert.Equal("170800", subject.Location.ArrivalScheduled.ToString())
		assert.Equal("170700", subject.Location.ArrivalActual.ToString())
		assert.Equal("170900", subject.Location.DepartureScheduled.ToString())
		assert.Equal("170800", subject.Location.DepartureActual.ToString())
		assert.Equal("Portsmouth Harbour", subject.Location.AllOriginNames())
		assert.Equal("Portsmouth Harbour", subject.Location.Origins[0].Name)
		assert.Equal("170100", subject.Location.Origins[0].Time.ToString())
		assert.Equal("London Victoria", subject.Location.AllDestinationNames())
		assert.Equal("London Victoria", subject.Location.Destinations[0].Name)
		assert.Equal("192100", subject.Location.Destinations[0].Time.ToString())
		assert.False(subject.Location.StartsHere())
		assert.False(subject.Location.TerminatesHere())
		assert.False(subject.Location.RightTimeArrival())
		assert.False(subject.Location.RightTimeDeparture())
		assert.False(subject.Location.IsCancelled())
	})

	test.Run("supports multiple destinations and starting here", func(test *testing.T) {
		subject := board.Services[3]
		assert.Equal("X14333", subject.UID)
		assert.Equal("1S31", subject.Headcode)
		assert.Equal("SN", subject.TOC)
		assert.Equal("Southern", subject.TOCName)
		assert.Equal("train", subject.ServiceType)
		assert.True(subject.IsPassenger)
		assert.False(subject.IsBus())
		assert.Equal("FTN", subject.Location.CRS)
		assert.Equal("2", subject.Location.Platform)
		assert.Equal(false, subject.Location.PlatformConfirmed)
		assert.Equal(false, subject.Location.PlatformChanged)
		assert.Equal("─", subject.Location.ArrivalScheduled.ToString())
		assert.Equal("─", subject.Location.ArrivalActual.ToString())
		assert.Equal("172200", subject.Location.DepartureScheduled.ToString())
		assert.Equal("172100", subject.Location.DepartureActual.ToString())
		assert.Equal("Fratton", subject.Location.AllOriginNames())
		assert.Equal("Fratton", subject.Location.Origins[0].Name)
		assert.Equal("172200", subject.Location.Origins[0].Time.ToString())
		assert.Equal("Brighton, Littlehampton", subject.Location.AllDestinationNames())
		assert.Equal("Brighton", subject.Location.Destinations[0].Name)
		assert.Equal("185200", subject.Location.Destinations[0].Time.ToString())
		assert.Equal("Littlehampton", subject.Location.Destinations[1].Name)
		assert.Equal("181400", subject.Location.Destinations[1].Time.ToString())
		assert.True(subject.Location.StartsHere())
		assert.False(subject.Location.TerminatesHere())
		assert.True(subject.Location.RightTimeArrival())
		assert.False(subject.Location.RightTimeDeparture())
		assert.False(subject.Location.IsCancelled())
	})

	test.Run("supports bus replacement services", func(test *testing.T) {
		subject := board.Services[4]
		assert.Equal("N11876", subject.UID)
		assert.Equal("0B00", subject.Headcode)
		assert.Equal("NT", subject.TOC)
		assert.Equal("Northern", subject.TOCName)
		assert.Equal("bus", subject.ServiceType)
		assert.True(subject.IsPassenger)
		assert.True(subject.IsBus())
		assert.Equal("NCL", subject.Location.CRS)
		assert.Equal("", subject.Location.Platform)
		assert.Equal(false, subject.Location.PlatformConfirmed)
		assert.Equal(false, subject.Location.PlatformChanged)
		assert.Equal("─", subject.Location.ArrivalScheduled.ToString())
		assert.Equal("─", subject.Location.ArrivalActual.ToString())
		assert.Equal("160000", subject.Location.DepartureScheduled.ToString())
		assert.Equal("─", subject.Location.DepartureActual.ToString())
		assert.Equal("Newcastle", subject.Location.AllOriginNames())
		assert.Equal("Newcastle", subject.Location.Origins[0].Name)
		assert.Equal("160000", subject.Location.Origins[0].Time.ToString())
		assert.Equal("Morpeth", subject.Location.AllDestinationNames())
		assert.Equal("Morpeth", subject.Location.Destinations[0].Name)
		assert.Equal("164000", subject.Location.Destinations[0].Time.ToString())
		assert.True(subject.Location.StartsHere())
		assert.False(subject.Location.TerminatesHere())
		assert.True(subject.Location.RightTimeArrival())
		assert.False(subject.Location.RightTimeDeparture())
		assert.False(subject.Location.IsCancelled())
	})

	test.Run("supports cancelled services", func(test *testing.T) {
		subject := board.Services[5]
		assert.Equal("S02693", subject.UID)
		assert.Equal("2N69", subject.Headcode)
		assert.Equal("LM", subject.TOC)
		assert.Equal("West Midlands Trains", subject.TOCName)
		assert.Equal("train", subject.ServiceType)
		assert.True(subject.IsPassenger)
		assert.False(subject.IsBus())
		assert.Equal("TRI", subject.Location.CRS)
		assert.Equal("3", subject.Location.Platform)
		assert.Equal(false, subject.Location.PlatformConfirmed)
		assert.Equal(false, subject.Location.PlatformChanged)
		assert.Equal("225600", subject.Location.ArrivalScheduled.ToString())
		assert.Equal("225500", subject.Location.ArrivalActual.ToString())
		assert.Equal("225600", subject.Location.DepartureActual.ToString())
		assert.Equal("225600", subject.Location.DepartureScheduled.ToString())
		assert.Equal("London Euston", subject.Location.AllOriginNames())
		assert.Equal("London Euston", subject.Location.Origins[0].Name)
		assert.Equal("221800", subject.Location.Origins[0].Time.ToString())
		assert.Equal("Northampton", subject.Location.AllDestinationNames())
		assert.Equal("Northampton", subject.Location.Destinations[0].Name)
		assert.Equal("233500", subject.Location.Destinations[0].Time.ToString())
		assert.False(subject.Location.StartsHere())
		assert.False(subject.Location.TerminatesHere())
		assert.False(subject.Location.RightTimeArrival())
		assert.True(subject.Location.RightTimeDeparture())
		assert.True(subject.Location.IsCancelled())
	})
}

func TestEmptyBoard(test *testing.T) {
	godotenv.Overload(".env.test")
	assert := assert.New(test)

	TheTime = fakeClock{}
	HTTPClient = fakeEmptyConnection{}
	txn := newrelic.Transaction{}
	board := board("FTN", TheTime, HTTPClient, &txn)

	test.Run("parses the Location", func(test *testing.T) {
		subject := board

		assert.Equal("FTN", subject.Location.CRS)
		assert.Equal("Fratton", subject.Location.Name)
		assert.Equal(len(board.Services), 0)
	})
}

func TestBlankBoard(test *testing.T) {
	godotenv.Overload(".env.test")
	assert := assert.New(test)

	TheTime = fakeClock{}
	HTTPClient = fakeBlankConnection{}
	txn := newrelic.Transaction{}
	board := board("FTN", TheTime, HTTPClient, &txn)

	test.Run("supports a lack of Location", func(test *testing.T) {
		subject := board

		assert.Equal("FTN", subject.Location.CRS)
		assert.Equal("FTN", subject.Location.Name)
		assert.Equal(len(board.Services), 0)
	})
}
