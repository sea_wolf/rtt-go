package main

import (
	"crypto/tls"
	"encoding/json"
	"log"
	"os"
	"time"

	"gopkg.in/resty.v1"
	"github.com/newrelic/go-agent/v3/newrelic"
)

func (conn RealConnection) DownloadDepartures(credsMap map[string]string, txn *newrelic.Transaction) string {
	client := resty.New()
	client.SetTLSClientConfig(&tls.Config{InsecureSkipVerify: true})

	request := client.R()
	request.SetHeader("Accept", "application/json")
	request.SetBasicAuth(credsMap["username"], credsMap["password"])

	s := newrelic.StartExternalSegment(txn, request.RawRequest)
	response, _ := request.Get(credsMap["url"])
	s.Response = response.RawResponse
	s.End()

	return response.String()
}

func downloadDeparturesConfig(crs string, datetime time.Time) map[string]string {
	url := os.Getenv("RTT_API_BASE") +
		os.Getenv("RTT_API_PATH") +
		os.Getenv("RTT_API_DEPARTURES") +
		"/" + crs +
		"/" + datetime.Format("2006/01/02/1504")

	config := make(map[string]string)
	config["url"] = url
	config["username"] = os.Getenv("RTT_API_USERNAME")
	config["password"] = os.Getenv("RTT_API_PASSWORD")

	return config
}

func departures(crs string, datetime time.Time, httpClient Connection, txn *newrelic.Transaction) DepartureResponse {
	config := downloadDeparturesConfig(crs, datetime)
	body := httpClient.DownloadDepartures(config, txn)

	if os.Getenv("DEBUG") != "" {
		log.Printf(body)
	}

	segmentRTTParsing := txn.StartSegment("RTT - Parsing")

	var response DepartureResponse
	if len(body) > 0 {
		if jsonError := json.Unmarshal([]byte(body), &response); jsonError != nil {
			panic("" + body + "\n" + jsonError.Error())
		}
	} else {
		location := JourneyPointSkeleton{CRS: crs, Name: crs}
		response = DepartureResponse{Time: datetime, Location: location}
	}

	response.Time = datetime

	segmentRTTParsing.End()
	return response
}
