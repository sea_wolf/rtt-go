package main

import "github.com/newrelic/go-agent/v3/newrelic"

type Connection interface {
	DownloadDepartures(map[string]string, *newrelic.Transaction) string
}

type RealConnection struct{}
