package main

import (
	"strings"
)

type JourneyPointSkeleton struct {
	CRS  string `json:"crs"`
	Name string `json:"name"`
}

type JourneyPoint struct {
	JourneyPointSkeleton

	Name string     `json:"description"`
	Time TimeHHMMSS `json:"workingTime,string"`
}

type CallingPoint struct {
	JourneyPointSkeleton

	Name              string `json:"description"`
	Platform          string `json:"platform"`
	PlatformConfirmed bool   `json:"platformConfirmed"`
	PlatformChanged   bool   `json:"platformChanged"`

	ArrivalScheduled   TimeHHMMSS `json:"gbttBookedArrival,string"`
	ArrivalActual      TimeHHMMSS `json:"realtimeArrival,string"`
	Cancellation       string     `json:"cancelReasonShortText"`
	DepartureScheduled TimeHHMMSS `json:"gbttBookedDeparture,string"`
	DepartureActual    TimeHHMMSS `json:"realtimeDeparture,string"`

	Origins      []JourneyPoint `json:"origin"`
	Destinations []JourneyPoint `json:"destination"`
}

func (location CallingPoint) AllOriginNames() string {
	return joinNames(location.Origins)
}

func (location CallingPoint) AllDestinationNames() string {
	return joinNames(location.Destinations)
}

func joinNames(locations []JourneyPoint) string {
	joined := ""
	joiner := ", "

	for _, location := range locations {
		joined += location.Name + joiner
	}

	return strings.TrimSuffix(joined, joiner)
}

func (location CallingPoint) StartsHere() bool {
	return matchesName(location.Origins, location)
}
func (location CallingPoint) TerminatesHere() bool {
	return matchesName(location.Destinations, location)

}
func matchesName(locations []JourneyPoint, location CallingPoint) bool {
	for _, thisLocation := range locations {
		if thisLocation.Name == location.Name {
			return true
		}
	}
	return false
}

func (location CallingPoint) RightTimeArrival() bool {
	return location.ArrivalScheduled == location.ArrivalActual
}
func (location CallingPoint) RightTimeDeparture() bool {
	return location.DepartureScheduled == location.DepartureActual
}
func (location CallingPoint) IsCancelled() bool {
	return len(location.Cancellation) > 0
}
func (location CallingPoint) delayMinutes() float64 {
	if location.ScheduledOnly() {
		return 0.0
	}

	return location.DepartureActual.Time.Sub(location.DepartureScheduled.Time).Minutes()
}
func (location CallingPoint) IsRightTime() bool {
	return location.delayMinutes() < 3
}
func (location CallingPoint) IsLightlyDelayed() bool {
	return location.delayMinutes() >= 3 && location.delayMinutes() < 5
}
func (location CallingPoint) IsHeavilyDelayed() bool {
	return location.delayMinutes() >= 5
}

func (location CallingPoint) ScheduledOnly() bool {
	return location.DepartureActual.Time.IsZero() || location.ArrivalActual.Time.IsZero()
}
