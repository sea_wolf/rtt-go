package main

import "time"

type Clock interface {
	Format(fmt string) string
	In(location *time.Location) time.Time
	SetTime(newTime time.Time)
	Time() time.Time
}

type RealClock struct {
	time time.Time
}

func (c RealClock) Format(fmt string) string {
	return c.time.Format(fmt)
}

func (c RealClock) In(location *time.Location) time.Time {
	return c.time.In(location)
}

func (c RealClock) SetTime(newTime time.Time) {
	c.time = newTime
}

func (c RealClock) Time() time.Time {
	return c.time
}
