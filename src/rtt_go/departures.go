package main

import (
	"strings"
	"time"
)

// DepartureResponse is essentially a departure board at a station
type DepartureResponse struct {
	Time     time.Time
	Location JourneyPointSkeleton `json:"location"`
	Services []Departure          `json:"services"`
}

// Departure is a service provided by a TOC
type Departure struct {
	UID         string       `json:"serviceUid"`
	Headcode    string       `json:"trainIdentity"`
	TOC         string       `json:"atocCode"`
	TOCName     string       `json:"atocName"`
	ServiceType string       `json:"serviceType"`
	IsPassenger bool         `json:"isPassenger"`
	Location    CallingPoint `json:"locationDetail"`
}

func (departureResponse DepartureResponse) summaries() map[string]TrainOperatingCompany {
	tocSummaries := make(map[string]TrainOperatingCompany, 0)
	for _, service := range departureResponse.Services {
		tocName := service.TOCName

		if _, exists := tocSummaries[tocName]; !exists {
			tocSummaries[tocName] = TrainOperatingCompany{Name: tocName}
		}

		toc := tocSummaries[tocName]
		if !service.Location.ScheduledOnly() {
			toc.ServiceCount++
			toc.DelayMins += float64(service.Location.delayMinutes())

			if service.Location.IsCancelled() {
				toc.Summary.Cancellations = append(toc.Summary.Cancellations, service)
			} else if service.Location.IsHeavilyDelayed() {
				toc.Summary.HeavilyDelayed = append(toc.Summary.HeavilyDelayed, service)
			} else if service.Location.IsLightlyDelayed() {
				toc.Summary.LightlyDelayed = append(toc.Summary.LightlyDelayed, service)
			} else if service.Location.IsRightTime() {
				toc.Summary.RightTime = append(toc.Summary.RightTime, service)
			}
		}
		tocSummaries[tocName] = toc
	}

	return tocSummaries
}

// IsBus is true when the service is operated with a bus (rather than a train, ship etc.)
func (departure Departure) IsBus() bool {
	return strings.ToUpper(departure.ServiceType) == "BUS"
}

// TrainOperatingCompany that operate a service
type TrainOperatingCompany struct {
	Name         string
	DelayMins    float64
	ServiceCount int
	Summary      Summary
}

// AvgDelayMins is the TOC's total number of delay minutes divided by the number of services
func (toc TrainOperatingCompany) AvgDelayMins() int {
	if toc.ServiceCount < 1 {
		return 0
	}

	return int(toc.DelayMins / float64(toc.ServiceCount))
}

// HasCancellations is true if any of the TOC's services are cancelled
func (toc TrainOperatingCompany) HasCancellations() bool {
	return len(toc.Summary.Cancellations) > 0
}

type Summary struct {
	Cancellations  []Departure
	HeavilyDelayed []Departure
	LightlyDelayed []Departure
	RightTime      []Departure
}
