package main

import "strings"

func timeWithColon(time string) string {
	if &time == nil {
		return ""
	}
	if len(time) == 0 {
		return ""
	}

	if strings.Index(time, ":") > -1 {
		return time
	}

	digits := strings.Split(time, "")
	joined := digits[0] + digits[1] + ":" + digits[2] + digits[3]

	return joined
}
