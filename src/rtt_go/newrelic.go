package main

import (
	"log"
	"os"

	"github.com/newrelic/go-agent/v3/newrelic"
)

func newrelicApp() *newrelic.Application {
	appName := os.Getenv("NEWRELIC_APP_NAME")
	licenseKey := os.Getenv("NEWRELIC_LICENSE_KEY")
	log.Printf("Starting NewRelic app %s with license %s", appName, licenseKey)
	app, err := newrelic.NewApplication(
		newrelic.ConfigAppName(appName),
		newrelic.ConfigLicense(licenseKey),
		func(config *newrelic.Config) {
			config.ErrorCollector.RecordPanics = true
		},
	)

	if err != nil {
		log.Printf("Unable to start NewRelic monitoring: %s", err)
	}
	return app
}
